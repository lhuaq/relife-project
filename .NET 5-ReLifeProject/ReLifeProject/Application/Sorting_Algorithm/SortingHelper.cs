﻿using Infrastructure.Commons.Sorting_Algorithm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Sorting_Algorithm
{
    public class SortingHelper
    {
        public void SortingHelper_SelectSort(IList<int> data)
        {
            Sorting_Methods Sorting_Methods_Obj = new Sorting_Methods();
            Sorting_Methods_Obj.SelectSort(data);
        }
    }
}
