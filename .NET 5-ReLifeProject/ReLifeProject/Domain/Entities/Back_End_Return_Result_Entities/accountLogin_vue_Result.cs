﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Back_End_Return_Result_Entities
{
    public class accountLogin_vue_Result
    {
        public bool IsSuccess { get; set; }
        public string Token { get; set; }
        public DateTime Expiration { get; set; }
        public int Duration { get; set; }
    }
}
