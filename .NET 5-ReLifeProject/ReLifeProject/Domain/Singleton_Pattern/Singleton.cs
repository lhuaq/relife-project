﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Singleton_Pattern
{
    public class Singleton
    {
        private Singleton() { }
        public static Singleton _instance
        {
            get
            {
                if (instance == null)
                {
                    lock (SingletonLock)
                    {
                        instance = new Singleton();
                    }
                }
                return instance;
            }
        }

        private static Singleton instance = null;

        private static object SingletonLock = new object();//加锁，防止多线程
        public int Id { get; set; }
        public string Name { get; set; }
        public string ConnectionString { get; set; }
        public string CorsURLs { get; set; }

    }
}
