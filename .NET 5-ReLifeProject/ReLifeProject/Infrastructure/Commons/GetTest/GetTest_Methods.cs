﻿using Infrastructure.Commons.WebSocketAPI;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Commons.GetTest
{
    public class GetTest_Methods
    {
        public string[] SubtitleArray = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "g", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };

        public void LoopMission()
        {
            for (int i = 0; i < 9999; i++)
            {
                Random randomObj = new Random();
                int IDValue = randomObj.Next(1, 10000);
                WebSocketTestValue WebSocketTestValueItem = new WebSocketTestValue();
                WebSocketTestValueItem.ID = IDValue.ToString();
                WebSocketTestValueItem.DateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss fff");
                WebSocketTestValueItem.TestValue = "qwereetyzxca简体测试繁體測試" + IDValue.ToString();
                WebSocketTestValueItem.l001 = randomsubtitle();
                System.Threading.Thread.Sleep(200);
                WebSocketTestValueItem.l002 = randomsubtitle();
                System.Threading.Thread.Sleep(200);
                WebSocketTestValueItem.l003 = randomsubtitle();
                System.Threading.Thread.Sleep(200);
                WebSocketTestValueItem.l004 = randomsubtitle();
                System.Threading.Thread.Sleep(200);


                System.Threading.Thread.Sleep(2000);

                bool WobSocketSendValue = WebSocketAPI.WebsocketHandlerMiddleware.WebSocketSendValue(JsonConvert.SerializeObject(WebSocketTestValueItem));
                if (!WobSocketSendValue)
                {
                    break;
                }
            }
        }
        public string randomsubtitle()
        {
            string randomSubtitl = string.Empty;

            Random randomobj = new Random();
            int arrayindex = 0;
            int randomnum = 0;
            while (randomSubtitl.Length <= 5)
            {
                randomnum = randomobj.Next(0, SubtitleArray.Count() - 1);
                if (randomnum != arrayindex)
                {
                    arrayindex = randomnum;
                    randomSubtitl = string.Format("{0}{1}", randomSubtitl, SubtitleArray[arrayindex]);
                }
            }
            return randomSubtitl;
        }
        public void SendMessageStruct()
        {

            for (int i = 0; i <= 10000; i++)
            {
                SendMessage SendMessageItem = new SendMessage
                {
                    sendTime = "发送时间",
                    sendValue = "发送内容",
                    sendHeadPhoto = "发送头像",
                    sendPerson = "发送者",
                };

                System.Threading.Thread.Sleep(2000);

                bool WobSocketSendValue = WebSocketAPI.WebsocketHandlerMiddleware.WebSocketSendValue(JsonConvert.SerializeObject(SendMessageItem));
                if (!WobSocketSendValue)
                {
                    break;
                }
            }
        }
        public struct SendMessage
        {
            public string sendTime { get; set; }
            public string sendValue { get; set; }
            public string sendHeadPhoto { get; set; }
            public string sendPerson { get; set; }
        }
        public string GetIPList(HttpContext httpContextItem, HttpRequest Request)
        {
            var httpRequestItem = Request;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"RemoteIpAddress：{httpContextItem.Connection.RemoteIpAddress}");

            if (httpRequestItem.Headers.ContainsKey("X-Real-IP"))
            {
                sb.AppendLine($"X-Real-IP：{httpRequestItem.Headers["X-Real-IP"].ToString()}");
            }

            if (httpRequestItem.Headers.ContainsKey("X-Forwarded-For"))
            {
                sb.AppendLine($"X-Forwarded-For：{httpRequestItem.Headers["X-Forwarded-For"].ToString()}");
            }

            return sb.ToString();
        }
    }
}
