﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Commons.Pointer_Test
{
    public unsafe class Pointer_Methods
    {
        #region 在写不安全代码的时候需要加上unsafe
        //标记字段
        unsafe int* pAge;
        //标记方法
        unsafe void getType(int* a)
        {
            //标记代码段
            unsafe
            {
                int* pAbg;//声明指针语法
                int test_Value = 111;
                pAbg = &test_Value;
                Console.WriteLine(*pAbg);
            }
        }
        #endregion

        #region 指针的语法
        public unsafe int Test()
        {
            int* pWidth, pHeight;
            double* pResult;
            byte*[] pByte;
            //&:表示“取地址”，并把一个值数据类型转换为指针，例如：int转换为*int。这个运算称为【寻址运算符】
            //*：表示“获取地址内容”，把一个指针转换为一个值数据类型（例如：*float转换为float）。这个运算符被称为“间接寻址运算符”（有时称“取消引用运算符”）
            int a = 10;//声明一个值类型，给它赋值10
            int* pA, pB;//声明两个指针
            pA = &a;//取出值类型a的地址，赋值给指针pA
            pB = pA;//把指针pA的地址赋值给pB
            *pB = 20;//获取pB指向的地址内容，并赋值20
            Console.WriteLine(a);//输出20

            #region 解决警告
            pByte = new byte*[3];

            double x = 10;
            pResult = &x;
            //*pResult = 33;

            pWidth = pA;
            pHeight = &a;

            pAge = pB;
            Console.WriteLine(*pResult);
            Console.WriteLine(*pAge);

            #endregion
            return a;
        }
        #endregion
    }
}
