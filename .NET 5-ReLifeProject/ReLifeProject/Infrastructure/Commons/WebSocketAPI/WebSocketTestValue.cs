﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Commons.WebSocketAPI
{
    public class WebSocketTestValue
    {
        public string ID { get; set; }
        public string DateTime { get; set; }
        public string TestValue { get; set; }
        public string l001 { get; set; }
        public string l002 { get; set; }
        public string l003 { get; set; }
        public string l004 { get; set; }
    }
}
