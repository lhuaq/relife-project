﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Commons.WebSocketAPI
{
    public class WebsocketHandlerMiddleware
    {
        private readonly RequestDelegate _next;

        public WebsocketHandlerMiddleware(
          RequestDelegate next
          )
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Path == "/ws")
            {
                if (context.WebSockets.IsWebSocketRequest)
                {
                    WebSocket webSocket = await context.WebSockets.AcceptWebSocketAsync();
                    string clientId = Guid.NewGuid().ToString(); ;
                    var wsClient = new WebsocketClient
                    {
                        Id = clientId,
                        WebSocket = webSocket
                    };
                    try
                    {
                        await Handle(wsClient);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                        await context.Response.WriteAsync("closed");
                    }
                }
                else
                {
                    context.Response.StatusCode = 404;
                }
            }
            else
            {
                await _next(context);
            }
        }
        private async Task Handle(WebsocketClient webSocket)
        {
            WebsocketClientCollection.Add(webSocket);

            WebSocketReceiveResult result = null;
            do
            {
                var buffer = new byte[1024 * 1];
                result = await webSocket.WebSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
            }
            while (!result.CloseStatus.HasValue);
            WebsocketClientCollection.Remove(webSocket);
        }

        public static bool WebSocketSendValue(string SendValue)
        {
            if (WebsocketClientCollection._clients.Count <= 0)
            {
                return false;
            }

            foreach (var _clientsItem in WebsocketClientCollection._clients)
            {
                if (_clientsItem.WebSocket.State == WebSocketState.Open)
                {
                    _clientsItem.SendMessageAsync(SendValue);
                }
            }

            return true;
        }
    }
}
