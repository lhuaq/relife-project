﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.MySQL_Infrastructure.Dapper_Interface
{
    public interface IMySQLDapperHelper
    {
        void Function1();

        T ExecuteReaderReturnT<T>(string sql, object param = null, IDbTransaction transaction = null);
    }
}
