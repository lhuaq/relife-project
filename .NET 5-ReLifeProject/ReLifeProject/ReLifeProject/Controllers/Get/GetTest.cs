﻿using Application.Sorting_Algorithm;
using Domain.Entities;
using Domain.Entities.MySQL_Entities.RelifeProject;
using Domain.Singleton_Pattern;
using Infrastructure.Commons.Pointer_Test;
using Infrastructure.Commons.Sorting_Algorithm;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using ReLifeProject.WebSocketAPI;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ReLifeProject.Controllers.Get
{
    [ApiController]
    [Route("[controller]")]
    public class GetTest : ControllerBase
    {
        //public string[] SubtitleArray = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "g", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };

        //[EnableCors("AnotherPolicy")]
        [HttpGet("/GetRandomInt")]
        public int GetRandomInt()
        {
            Random random = new Random(BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0));
            int value = random.Next(int.MinValue, int.MaxValue);
            return value;
        }

        //[EnableCors("Policy1")]
        [HttpGet("/GetRandomBool")]
        public bool GetRandomBool()
        {
            Random random = new Random(BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0));
            int value = random.Next(int.MinValue, int.MaxValue);

            if (value < 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [Authorize]
        //[EnableCors("Vue3")]
        [HttpGet("/GetRandomString")]
        public string GetRandomString()
        {
            string[] RandomStrArray = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
            Random random = new Random(BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0));
            string RandomString = "";
            for (int i = 0; i <= 5; i++)
            {
                int value = random.Next(0, RandomStrArray.Count());
                RandomString += RandomStrArray[value];
            }
            //RandomString += ID;

            return RandomString;
        }

        //[EnableCors("Vue3")]
        [HttpGet("/GetWebSocket")]
        public string GetWebSocket()
        {
            string[] RandomStrArray = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
            Random random = new Random(BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0));
            string RandomString = "";
            for (int i = 0; i <= 5; i++)
            {
                int value = random.Next(0, RandomStrArray.Count());
                RandomString += RandomStrArray[value];
            }
            Task.Run(() =>
            {
                Infrastructure.Commons.GetTest.GetTest_Methods getTest_Methods = new Infrastructure.Commons.GetTest.GetTest_Methods();
                getTest_Methods.LoopMission();
            });

            return RandomString;
        }

        //[EnableCors("Vue3")]
        [HttpGet("/GetSendMessage")]
        public async Task<string> GetSendMessage()
        {
            string[] RandomStrArray = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
            Random random = new Random(BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0));
            string RandomString = "";
            for (int i = 0; i <= 5; i++)
            {
                int value = random.Next(0, RandomStrArray.Count());
                RandomString += RandomStrArray[value];
            }
            //RandomString += ID;

            await Task.Run(() =>
            {
                Infrastructure.Commons.GetTest.GetTest_Methods getTest_Methods = new Infrastructure.Commons.GetTest.GetTest_Methods();
                getTest_Methods.SendMessageStruct();
            });

            return RandomString;
        }

        //[EnableCors("Vue3")]
        [HttpGet("/GetIP")]
        public async Task<ActionResult<IEnumerable<string>>> GetIP()
        {
            Infrastructure.Commons.GetTest.GetTest_Methods getTest_Methods = new Infrastructure.Commons.GetTest.GetTest_Methods();
            var iPValue = await Task.Run(() => getTest_Methods.GetIPList(this.HttpContext, this.Request));

            return Ok(iPValue);
        }

        //[EnableCors("Vue3")]
        [HttpGet("/GetSelectSort")]
        public async Task<string> GetSelectSort()
        {
            List<int> data = new List<int>();

            Random random = new Random(BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0));
            for (int i = 0; i < 100000; i++)
            {
                var addValue = random.Next(0, int.MaxValue);
                data.Add(addValue);
            }

            List<int> xuanpai = new List<int>(data);
            List<int> kuaipai = new List<int>(data);
            List<int> kuaipaiwiki = new List<int>(data);

            #region 选择排序
            SortingHelper sortingHelper = new SortingHelper();
            Sorting_Methods Sorting_Methods_Obj = new Sorting_Methods();
            Sorting_Methods_Obj.SelectSort(xuanpai);
            await Task.Run(() => sortingHelper.SortingHelper_SelectSort(xuanpai));
            #endregion

            #region 指针测试
            Pointer_Methods pointer_Methods = new Pointer_Methods();
            Console.WriteLine("指针测试内容为：" + pointer_Methods.Test());
            #endregion

            #region 快速排序

            await Task.Run(() =>
            {
                Sorting_Methods_Obj.QuickSortStrict(kuaipai, 0, kuaipai.Count - 1); Console.WriteLine("Start");
                StringBuilder stringBuilder = new StringBuilder();
                foreach (var data_Item in kuaipai)
                {
                    stringBuilder.Append(data_Item.ToString() + " ");
                }
                Console.WriteLine(stringBuilder + "\r\n");
            });
            #endregion

            #region 快速排序（维基版）

            await Task.Run(() =>
            {
                Sorting_Methods_Obj.QuickSortRelax(kuaipaiwiki, 0, kuaipaiwiki.Count - 1);
                Console.WriteLine("Start");
                StringBuilder stringBuilder = new StringBuilder();
                foreach (var data_Item in kuaipaiwiki)
                {
                    stringBuilder.Append(data_Item.ToString() + " ");
                }
                Console.WriteLine(stringBuilder + "\r\n");
            });
            #endregion

            return "ok";
        }

        //[EnableCors("Vue3")]
        [HttpGet("/GetSingletonFirst")]
        [Authorize]
        public async Task<int> GetSingletonFirst()
        {
            Singleton singleton = Singleton._instance;
            singleton.Id = ++singleton.Id;
            singleton.Name = "测试Name";

            await Task.Run(() =>
            {
                Console.WriteLine(singleton.Id);
                Console.WriteLine(singleton.Name);
                Console.WriteLine(singleton.ConnectionString);
            });

            return singleton.Id;
        }

        //[EnableCors("Vue3")]
        [HttpGet("/GetSingletonSecond")]
        public async Task<int> GetSingletonSecond()
        {
            Singleton singleton = Singleton._instance;

            await Task.Run(() =>
            {
                Console.WriteLine(singleton.Id);
                Console.WriteLine(singleton.Name);
                Console.WriteLine(singleton.ConnectionString);
            });

            return singleton.Id;
        }

        //[EnableCors("Vue3")]
        [HttpPost("/PostLogin")]
        public async Task<IActionResult> PostLogin([FromBody] LoginInput input)
        {

            //从数据库验证用户名，密码
            //验证通过 否则 返回Unauthorized

            //创建claim
            var authClaims = new[] {
                new Claim(JwtRegisteredClaimNames.Sub,input.Username),
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString())
            };
            IdentityModelEventSource.ShowPII = true;
            //签名秘钥 可以放到json文件中
            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("SecureKeySecureKeySecureKeySecureKeySecureKeySecureKey"));

            var token = new JwtSecurityToken(
                   issuer: "http://localhost:13164",
                   audience: "http://localhost:13164",
                   expires: DateTime.Now.AddHours(2),
                   claims: authClaims,
                   signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                   );

            await Task.Run(() =>
            {
                Console.WriteLine(token);
            });

            //返回token和过期时间
            return Ok(new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token),
                expiration = token.ValidTo,
                duration = 3000
            });
        }

        [Authorize]
        [HttpPost("/PostMySQLTest")]
        public string PostMySQLTest([FromBody]check checkValue)
        {
            var httpRequestItem = Request;
            
            Singleton singleton = Singleton._instance;
            test testsql = new test { testid = Guid.NewGuid().ToString("N"), testtime = DateTime.Now, testcontent = "测试", testnum = 1 };
            MySqlConnection connection = new MySqlConnection(singleton.ConnectionString);
            MySqlCommand command = new MySqlCommand();
            switch (checkValue.type)
            {
                case "0":
                    command = new MySqlCommand("select * from test", connection);
                    connection.Open();
                    MySqlDataAdapter adapter = new MySqlDataAdapter(command);
                    connection.Close();
                    Console.WriteLine("查询信息成功");
                    break;
                case "1":
                    connection.Open();
                    string sql = "insert into test(testid,testtime,testcontent,testnum) values ('" + testsql.testid + "','" + testsql.testtime + "','" + testsql.testcontent + "','" + testsql.testnum + "')";
                    command = new MySqlCommand(sql, connection);
                    int result = command.ExecuteNonQuery();
                    Console.WriteLine("插入信息成功");
                    connection.Close();
                    break;
                default: Console.WriteLine("错误值"); break;
            }
            return "good";
        }








        #region 实现逻辑转至基础设施层
        //private void LoopMission()
        //{
        //    for (int i = 0; i < 9999; i++)
        //    {
        //        Random randomObj = new Random();
        //        int IDValue = randomObj.Next(1, 10000);
        //        WebSocketTestValue WebSocketTestValueItem = new WebSocketTestValue();
        //        WebSocketTestValueItem.ID = IDValue.ToString();
        //        WebSocketTestValueItem.DateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss fff");
        //        WebSocketTestValueItem.TestValue = "qwereetyzxca简体测试繁體測試" + IDValue.ToString();
        //        WebSocketTestValueItem.l001 = randomsubtitle();
        //        System.Threading.Thread.Sleep(200);
        //        WebSocketTestValueItem.l002 = randomsubtitle();
        //        System.Threading.Thread.Sleep(200);
        //        WebSocketTestValueItem.l003 = randomsubtitle();
        //        System.Threading.Thread.Sleep(200);
        //        WebSocketTestValueItem.l004 = randomsubtitle();
        //        System.Threading.Thread.Sleep(200);


        //        System.Threading.Thread.Sleep(2000);

        //        bool WobSocketSendValue = WebSocketAPI.WebsocketHandlerMiddleware.WebSocketSendValue(JsonConvert.SerializeObject(WebSocketTestValueItem));
        //        if (!WobSocketSendValue)
        //        {
        //            break;
        //        }
        //    }
        //}
        //private string randomsubtitle()
        //{
        //    string randomSubtitl = string.Empty;

        //    Random randomobj = new Random();
        //    int arrayindex = 0;
        //    int randomnum = 0;
        //    while (randomSubtitl.Length <= 5)
        //    {
        //        randomnum = randomobj.Next(0, SubtitleArray.Count() - 1);
        //        if (randomnum != arrayindex)
        //        {
        //            arrayindex = randomnum;
        //            randomSubtitl = string.Format("{0}{1}", randomSubtitl, SubtitleArray[arrayindex]);
        //        }
        //    }
        //    return randomSubtitl;
        //}
        //private void SendMessageStruct()
        //{

        //    for (int i = 0; i <= 10000; i++)
        //    {
        //        SendMessage SendMessageItem = new SendMessage
        //        {
        //            sendTime = "发送时间",
        //            sendValue = "发送内容",
        //            sendHeadPhoto = "发送头像",
        //            sendPerson = "发送者",
        //        };

        //        System.Threading.Thread.Sleep(2000);

        //        bool WobSocketSendValue = WebSocketAPI.WebsocketHandlerMiddleware.WebSocketSendValue(JsonConvert.SerializeObject(SendMessageItem));
        //        if (!WobSocketSendValue)
        //        {
        //            break;
        //        }
        //    }
        //}
        //public struct SendMessage
        //{
        //    public string sendTime { get; set; }
        //    public string sendValue { get; set; }
        //    public string sendHeadPhoto { get; set; }
        //    public string sendPerson { get; set; }
        //}
        //private string GetIPList(HttpContext httpContextItem)
        //{
        //    var httpRequestItem = Request;
        //    StringBuilder sb = new StringBuilder();
        //    sb.AppendLine($"RemoteIpAddress：{httpContextItem.Connection.RemoteIpAddress}");

        //    if (httpRequestItem.Headers.ContainsKey("X-Real-IP"))
        //    {
        //        sb.AppendLine($"X-Real-IP：{httpRequestItem.Headers["X-Real-IP"].ToString()}");
        //    }

        //    if (httpRequestItem.Headers.ContainsKey("X-Forwarded-For"))
        //    {
        //        sb.AppendLine($"X-Forwarded-For：{httpRequestItem.Headers["X-Forwarded-For"].ToString()}");
        //    }

        //    return sb.ToString();
        //}
        #endregion
    }

    public class check
    {
        public string type { get; set; }
    }
}
