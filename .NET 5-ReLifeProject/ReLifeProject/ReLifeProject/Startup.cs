using Domain.Entities;
using Domain.Singleton_Pattern;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReLifeProject
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region 读取appsettings.json并且设置连接字符串至单例类中
            var Logging = Configuration.GetSection("Logging");
            var AllowedHosts = Configuration.GetValue("AllowedHosts", "数值为空");
            var ConnectionString = Configuration.GetValue("ConnectionString", "数值为空");
            var DBConnectionString = Configuration.GetSection("DBConnectionString");
            var CorsURLs = Configuration.GetValue("CorsURLs", "http://localhost:8080,http://139.196.222.182:1001");

            Singleton singleton = Singleton._instance;
            singleton.ConnectionString = ConnectionString;
            singleton.CorsURLs = CorsURLs;
            #endregion

            #region 控制器
            services.AddControllers();
            #endregion

            #region Api阅览列表
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "ReLifeProject", Version = "v1" });
                //JWT验证相关
                /**/
                var securityScheme = new OpenApiSecurityScheme
                {
                    Name = "JWT Authentication",
                    Description = "Enter JWT Bearer token **_only_**",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.Http,
                    Scheme = "bearer",
                    BearerFormat = "JWT",
                    Reference = new OpenApiReference
                    {
                        Id = JwtBearerDefaults.AuthenticationScheme,
                        Type = ReferenceType.SecurityScheme
                    }
                };
                c.AddSecurityDefinition(securityScheme.Reference.Id, securityScheme);
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    { securityScheme, new string[] { } }
                });
            });
            #endregion

            #region Cors跨域
            services.AddCors(options =>
            {
                /*options.AddPolicy("Policy1",
                    builder =>
                    {
                        builder.WithOrigins("http://example.com",
                                            "http://www.contoso.com");
                    });

                options.AddPolicy("AnotherPolicy",
                    builder =>
                    {
                        builder.WithOrigins("http://www.contoso.com")
                                            .AllowAnyHeader()
                                            .AllowAnyMethod()
                                            .AllowCredentials();
                    });

                options.AddPolicy("Vue3",
                    builder =>
                    {
                        builder.WithOrigins("http://localhost:3000", "http://192.168.1.101:3000",
                            "http://localhost:8080", "http://192.168.1.101:8080",
                            "http://localhost:8081", "http://192.168.1.101:8081",
                            "http://localhost:13164", "http://192.168.1.101:13164",
                            "https://www.cnblogs.com/chengtian", "http://localhost:1",
                            "http://127.0.0.1")
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowCredentials();
                    });*/
                options.AddDefaultPolicy(
                    builder =>
                    {
                        //builder.SetIsOriginAllowed(origin => new Uri(origin).Host == "localhost");
                        builder.WithOrigins(singleton.CorsURLs.Split(","))
                        //添加预检请求过期时间
                         .SetPreflightMaxAge(TimeSpan.FromSeconds(2520))
                        //如果不需要跨域请注释掉.AllowCredentials()或者增加跨域策略
                        .AllowCredentials()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                    });
            });
            /*// 配置跨域处理，允许所有来源
            services.AddCors(options => options.AddPolicy("CorsPolicy",
            builder =>
            {
                builder.AllowAnyMethod()
                    .AllowAnyHeader()
                    .SetIsOriginAllowed(_ => true) // =AllowAnyOrigin()
                    .AllowCredentials();
            }));*/
            #endregion

            #region JWT认证与授权
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidAudience = "http://localhost:13164",
                    ValidIssuer = "http://localhost:13164",
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("SecureKeySecureKeySecureKeySecureKeySecureKeySecureKey"))
                };
            });
            //"https://www.cnblogs.com/chengtian","http://localhost:8080",
            //"https://www.cnblogs.com/chengtian","http://localhost:8080",
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            #region Api阅览列表
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ReLifeProject v1"));
            }
            #endregion

            //app.UseHttpsRedirection();https://www.cnblogs.com/bodom/p/9896264.html

            app.UseRouting();

            #region 开启认证
            app.UseAuthentication();//开启认证
            #endregion

            #region 授权中间件
            app.UseAuthorization();//授权中间件
            #endregion

            #region Cors跨域
            app.UseCors();
            // 允许所有跨域，cors是在ConfigureServices方法中配置的跨域策略名称
            //app.UseCors("CorsPolicy");//（增加这一行，注意位置）
            #endregion

            #region WebSocket前后端实时通讯
            app.UseWebSockets();
            app.UseMiddleware<Infrastructure.Commons.WebSocketAPI.WebsocketHandlerMiddleware>();
            #endregion

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
