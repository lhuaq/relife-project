# RelifeProject

#### Description
后端技术栈：.net 5框架； websocket通讯；  DDD领域驱动设计模式（用户接口层、应用层、领域层、基础设施层）、单例模式； JwtBear认证与授权； Newtonsoft.Json转换Json对象；  Swagger；
前端技术栈：Vue3框架； Three.js； webpack； websocket； Cookies； echarts；Axios；Vue-Router；

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
