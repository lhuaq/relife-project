import axios from 'axios';
import cookies from 'vue-cookies';

//创建axios的一个实例 
var instance = axios.create({
    baseURL: 'http://139.196.222.182:5000/',//http://localhost:13164/接口统一域名
    timeout: 60000                                                       //设置超时
})
//------------------- 一、请求拦截器 忽略
instance.interceptors.request.use(config => {
    const token = cookies.get("token");
    if (token) {
        config.headers['Authorization'] = 'Bearer ' + token; // 让每个请求携带自定义token 请根据实际情况自行修改
    }
    return config;
},
    error => {
        console.log(error)
        // 对请求错误做些什么
        return Promise.reject(error);
    });

//----------------- 二、响应拦截器 忽略
instance.interceptors.response.use(function (response) {
    return response.data;
}, function (error) {
    // 对响应错误做点什么
    console.log('拦截器报错');
    return Promise.reject(error);
});

export default instance
