import messageTips from '../messageTipsCOMPO/messageTips.js'
import req from '../../api/axiosPackage.js';

export function PostaccountLogin({ accountName, accountPassWord }) {
    return req({
        url: '/PostaccountLogin',
        method: 'post',
        data: {
            accountName: accountName,
            accountPassWord: accountPassWord
        },
    })
}


export async function testAsync() {//await testAwait()
    //const time1 = 300;
    //const time2 = await step1(time1);
    //const time3 = await step2(time1, time2);
    //const result = await step3(time1, time2, time3);
    //console.log(`result is ${result}`);
    successTips();
}

function successTips() {
    return messageTips({ text: '登录成功', type: 'success' });
}

/*function warnTips() {
    return messageTips({ text: '登录警告', type: 'warn' });
}

function errorTips() {
    return messageTips({ text: '登录失败', type: 'error' });
}*/

/*function step1(n) {
    console.log(`step1 with ${n}`);
    return takeLongTime(n);
}

function step2(m, n) {
    console.log(`step2 with ${m} and ${n}`);
    return takeLongTime(m + n);
}

function step3(k, m, n) {
    console.log(`step3 with ${k}, ${m} and ${n}`);
    return takeLongTime(k + m + n);
}

function takeLongTime(n) {
    return new Promise(resolve => {
        setTimeout(() => resolve(n + 200), n);
    });
}*/