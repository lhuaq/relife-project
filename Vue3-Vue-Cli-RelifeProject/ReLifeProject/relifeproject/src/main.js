import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import VueCookies from 'vue-cookies'
import VueParticles from 'vue-particles'
import Message from './components/messageTipsCOMPO/messageTips.js'
//import './style/resrt.css'


const app = createApp(App);
app.use(router);
app.use(VueParticles);
app.config.globalProperties.$message = Message
app.config.globalProperties.$cookies = VueCookies;
app.mount('#app');

/*createApp(App).mount('#app')*/