import { createRouter, createWebHistory } from 'vue-router'
import cookies from 'vue-cookies';
//import Home from '../views/home/home.vue'
//import Contact from '../views/communication/Contact.vue'
//import Login from '../views/login/Login.vue'

/*const routerHistory = createWebHistory()

const router = createRouter({
    history: routerHistory,
    routes: [
      {
        path: '/',
        component: Home
      },
      {
        path: '/contact',
        component: Contact
      },
      {
        path:'/Login',
        component:Login
      }
    ]
  })
  
export default router*/

const routes = [
  {
    path: '/home',
    component: () => import('../views/home/home.vue'),
    meta: {
      title: 'RelifeProject~主页'
    }
  },
  {
    path: '/communication',
    component: () => import('../views/communication/communication.vue'),
    meta: {
      title: 'RelifeProject~联系页面'
    }
  },
  {
    path: '/login',
    component: () => import('../views/login/login.vue'),
    children: [
      {
        path: '/accountLogin',
        component: () => import('../components/accountLoginCOMPO/accountLogin.vue'),
        meta: {
          title: 'RelifeProject~登陆页面'
        }
      },
      {
        path: '/accountRegister',
        component: () => import('../components/accountRegisterCOMPO/accountRegister.vue'),
        meta: {
          title: 'RelifeProject~注册页面'
        }
      },
    ],
  },
  {
    path: '/error',
    component: () => import('../views/error/error.vue'),
    meta: {
      title: 'RelifeProject~错误页面'
    }
  },
]

const router = createRouter({
  history: createWebHistory(),
  scrollBehavior: () => ({ y: 0 }),
  routes: routes
})


//路由守卫
router.beforeEach((to, from, next) => {
  if (to.meta.title) {//如果设置标题，拦截后设置标题
    document.title = to.meta.title
  }
  const token = cookies.get("token");
  //const token = localStorage.getItem('stor')
  // const role = localStorage.getItem('ms_username');
  // NProgress.start(); //进度条
  /*if (to.path !== '/Login') {//if (to.path !== '/login' && !token) {
    console.log('1')
    next('/Login');
  } else {
    console.log('2')
    if (to.name === 'Login') {//if (to.name === 'Login' && token) {
      console.log('3')
      router.push('/')
    } else {
      console.log('4')
      next();
    }    
  }*/
  if (!token) {
    if (to.path !== '/login') {
      if (to.path === '/accountLogin' || to.path === '/accountRegister') {
        next();
      } else {
        next('/login');
      }
    } else {
      next();
    }
  }
  else {
    if (to.path === '/login' || to.path === '/accountLogin' || to.path === '/accountRegister' || to.path === '/') {
      router.push('/home')
    }
    else {
      next();
    }
  }




  /*if (!token && to.path !== '/login') {
    if(to.path === '/accountLogin' || to.path === '/accountRegister'){
      next();
    }else{
      next('/login');
    }    
  } else {
    if (token && (to.path === '/login' || to.path === '/accountLogin' || to.path === '/accountRegister')) {
      router.push('/home')
    } else {
      next();
    }
  }*/
});


export default router